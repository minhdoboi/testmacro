package priv

import shapeless.{ HList, HNil, ProductTypeClass, ProductTypeClassCompanion, :: }

object Zero extends ProductTypeClassCompanion[Zero] {

  implicit def numZero[A](implicit num: Numeric[A]) = new Zero[A] {
    def empty: A = num.zero
  }

  implicit def stringZero: Zero[String] = new Zero[String] {
    def empty = ""
  }

  object typeClass extends ProductTypeClass[Zero] {
    def emptyProduct = new Zero[HNil] {
      def empty = HNil
    }

    def product[F, T <: HList](mh: Zero[F], mt: Zero[T]) = new Zero[F :: T] {
      def empty = mh.empty :: mt.empty
    }

    def project[F, G](instance: ⇒ Zero[G], to: F ⇒ G, from: G ⇒ F) = new Zero[F] {
      def empty = from(instance.empty)
    }
  }
}

trait Zero[A] {
  def empty: A
}
