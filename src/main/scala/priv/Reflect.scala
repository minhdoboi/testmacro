package priv

import scala.reflect.ClassTag

trait DateBucketDefaultByReflection {

  implicit def genDateBucketDefault[A <: DateBucket](implicit zero: Zero[A], ct: ClassTag[A]): DateBucketDefault[A] = new DateBucketDefault[A] {

    def getDefault(date: String): A = {
      val value = zero.zero
      Reflect.updateParam(value, "date", date)
    }
  }

}
object Reflect {
    import scala.reflect._
    import scala.reflect.runtime.universe._
    import scala.reflect.runtime._

    def updateParam[R : ClassTag](r: R, paramName: String, paramValue: Any): R = {
        val instanceMirror = runtimeMirror(getClass.getClassLoader).reflect(r)
        val decl = instanceMirror.symbol.asType.toType
        val members = decl.members.flatMap{ method =>
            transformMethod(method, paramName, paramValue, instanceMirror)
        }
        .toArray.reverse

        val copyMethod = decl.declaration(newTermName("copy")).asMethod
        val copyMethodInstance = instanceMirror.reflectMethod(copyMethod)

        copyMethodInstance(members: _*).asInstanceOf[R]
    }

    def transformMethod(method: Symbol, paramName: String, paramValue: Any, instanceMirror: InstanceMirror) = {
        val term = method.asTerm
        if (term.isAccessor) {
            if (term.name.toString == paramName) {
                Some(paramValue)
            } else Some(instanceMirror.reflectField(term).get)
        } else None
    }
}