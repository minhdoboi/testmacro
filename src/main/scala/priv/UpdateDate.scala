package priv

import scala.language.experimental.macros

trait DateBucketDefaultByMacro {
  implicit def genDateBucketDefault[A <: DateBucket](implicit zero: Zero[A], up : UpdateDate[A]): DateBucketDefault[A] = {
    new DateBucketDefault[A]{
      def getDefault(date: String): A = {
        up.setDate(zero.empty, date)
      }
    }
  }
}

object UpdateDate {
  import scala.reflect.macros._

  implicit def genUpdateDate[A <: DateBucket]: UpdateDate[A] = macro GenUpdateDate.impl[A]

  object GenUpdateDate {

    def impl[A <: DateBucket: c.WeakTypeTag ](c: blackbox.Context) : c.Expr[UpdateDate[A]] = {
      import c.universe._
      val tpe = c.weakTypeOf[A]

      c.Expr[UpdateDate[A]](
        q"""new UpdateDate[${tpe}] {

          def setDate(value : ${tpe}, date: String): ${tpe} = {
            value.copy(date=date)
          }
      }""")
    }
  }
}


trait UpdateDate[A <: DateBucket] {
  def setDate(value : A, date: String): A
}
