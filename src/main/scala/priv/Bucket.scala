package priv


case class Bucket[A](key: String, value: A) extends DateBucket {
  def date = key
}

trait DateBucket {
  def date: String
}

object DateBucketDefault {
  implicit def genDateBucketDefault[A <: DateBucket](implicit zero: Zero[A], up : UpdateDate[A]): DateBucketDefault[A] = {
    new DateBucketDefault[A]{
      def getDefault(date: String): A = {
        up.setDate(zero.zero, date)
      }
    }
  }
}


trait DateBucketDefault[A <: DateBucket] {
  def getDefault(date: String): A
}

