package priv

object Main extends App {

    val result = testBucket[MyBucket]
    println(result)


    def testBucket[A <: DateBucket](implicit default : DateBucketDefault[A]) : A = {
        default.getDefault("2018-01")
    }
}



case class MyBucket(
    date : String,
    i : Int
) extends DateBucket

